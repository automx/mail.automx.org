" {{ ansible_managed }}

set background=light

syntax enable

set cursorline
hi CursorLine   cterm=NONE ctermbg=darkgray ctermfg=white guibg=darkred guifg=white
hi CursorColumn cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white
nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>


if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
  autocmd filetype mail set notitle
  filetype plugin indent on
	autocmd filetype css setlocal equalprg=csstidy\ -\ --silent=true\ --compress_font-weight=false
endif

set showcmd
set showmatch
set smartcase
set ignorecase
set incsearch
set autowrite
set hidden

set autoindent
set modeline
set pastetoggle=<F8>
set ruler
set shortmess+=I
set mps=(:),{:},[:],<:>
set title
set titlestring=%F
set listchars=tab:»·,trail:·,eol:¬
set nolist
map <F5> :set list!<CR>
set nojs
set laststatus=2

" Tabbing
set tabstop=4
set shiftwidth=4
set expandtab

nmap <C-j> gq<CR>

let mapleader = "\<Space>"
nnoremap <Leader>j gq<CR>

ab ANMA # {{ ansible_managed }}

" vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
