# {{ ansible_managed }}
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias l='ls -lhA --color=auto'
alias ll='ls -lha --color=auto'

PS1='[\u@\h \W]\$ '
